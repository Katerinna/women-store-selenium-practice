package TestUtils;

import io.cucumber.messages.types.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;


public class Base {

    WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), Duration.ofMinutes(1));

    JavascriptExecutor javascriptexecutor;
    Actions actions;
    PropertyReader prop;
    WebDriver driver = DriverManager.getDriver();
    private static final String CLICK_COMMAND = "arguments[0].click();";


    /**
     * This is to click on Webelement
     *
     * @param e
     */
    public void clickElement(WebElement e) {
        if (e.isDisplayed()) {
            e.click();
        } else {
            wait.until(ExpectedConditions.visibilityOf(e));
            e.click();
        }
    }

    /**
     * To mouse hover element and click on it
     *
     * @param element
     */
    public void mouseHoverClick(WebElement element) {
        actions = new Actions(DriverManager.getDriver());
        actions.click(element).build().perform();
    }


    public void elementWaitVisibility(WebElement e) {
        wait.until(ExpectedConditions.visibilityOf(e));
    }

    public void scrollDown(WebElement webElement) {
        actions = new Actions(DriverManager.getDriver());
        actions.sendKeys(Keys.PAGE_DOWN).build().perform();

    }

    public void elementWaitToBeClickable(WebElement webElement) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }
}


