package TestUtils;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class DriverManager {

   private static DriverManager driverManager = new DriverManager();


    public static DriverManager getInstance( ) {
        return driverManager;
    }

    public  WebDriver driver;
    private String Driver;
    private String driverType;
    private String headless;
    PropertyReader propertyReader = new PropertyReader();
    public static ThreadLocal<WebDriver> tlDriver=new ThreadLocal<>();


    public WebDriver init_driver(String browser)
    {
        if(browser.equalsIgnoreCase("chrome")){
            WebDriverManager.chromedriver().setup();
            tlDriver.set(new ChromeDriver());
        } else if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            tlDriver.set(new FirefoxDriver());
        }
        else {
            System.out.println("Please enter correct browser");
            tlDriver.set(null);
        }

        // driver.manage().timeouts().implicitlyWait(propertyReader.getImplicitlyWait(), TimeUnit.SECONDS);
        return getDriver();
    }

    public static synchronized WebDriver getDriver()
    {
        return  tlDriver.get();
    }

 /*   public WebDriver getDriver() {
        driverType = propertyReader.getDriver();
        if(driver == null)
            driver = createLocalDriver();
        return driver;
    }*/



   /* private WebDriver createLocalDriver() {
        Driver = propertyReader.getDriver();
        String headless = propertyReader.getHeadless();

        if(Driver.equalsIgnoreCase("FIREFOX")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();

        } else if (Driver.equalsIgnoreCase("CHROME")) {

            if (headless.equals("true")) {
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                options.addArguments("--no-sandbox");
                options.addArguments("disable-gpu");
                driver = new ChromeDriver(options);
            } else {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();

            }

        }


        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(propertyReader.getImplicitlyWait(), TimeUnit.SECONDS);
        return driver;
    }
*/
    public void closeDriver() {
        driver.close();
        driver.quit();
    }



}
