package TestUtils;

import java.io.*;
import java.util.Properties;

public class PropertyReader {


    public static Properties properties;
    public static final String propertyFilePath= "./src/test/java/resources/test.properties";

    public PropertyReader()  {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Test.properties not found at " + propertyFilePath);
        }
    }



    public String getDriverPath(){
        String driverPath = properties.getProperty("driverPath");
        if(driverPath!= null) return driverPath;
        else throw new RuntimeException("driverPath not specified in the test.properties file.");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
        else throw new RuntimeException("implicitlyWait not specified in the test.properties file.");
    }

    public String getPreviewUrl() {
        String previewUrl = properties.getProperty("previewUrl");
        if(previewUrl != null) return previewUrl;
        else throw new RuntimeException("previewUrl not specified in the test.properties file.");
    }

    public String getHeadless() {
        String headless = properties.getProperty("headless");
        if(headless != null) return headless;
        else throw new RuntimeException("headless not specified in the test.properties file.");
    }

    public String getEnvironment() {
        String environment = properties.getProperty("environment");
        if(environment != null) return environment;
        else throw new RuntimeException("environment not specified in the test.properties file.");
    }


}
