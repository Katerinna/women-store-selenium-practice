package Stepdefinition;

import TestUtils.Base;
import TestUtils.DriverManager;
import TestUtils.PropertyReader;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageobjects.CheckoutPage;
import pageobjects.Login;
import pageobjects.PageObjectManager;
import pageobjects.Women;

import java.io.IOException;
import java.time.Duration;

public class WomenStepdefTest extends Base {
    Women women;
    PropertyReader propertyReader;
    PageObjectManager pageObjectManager;
    DriverManager drivermanager;
    @Given("On Women Page Add the Printed Chiffon Dress in the cart")
    public void on_women_page_add_the_printed_chiffon_dress_in_the_cart() {
        pageObjectManager= PageObjectManager.getInstance();
        women=pageObjectManager.getWomenPage(DriverManager.getDriver());
        women.navigateToWomenPage();

        women.addPrintedChiffonDressInTheCart();

    }
    @Then("Verify that Product successfully added to your shopping cart")
    public void verify_that_product_successfully_added_to_your_shopping_cart() {
        women.productSuccessfullyAddedToShoppingCart();

    }


}
