package Stepdefinition;

import TestUtils.Base;
import TestUtils.DriverManager;
import TestUtils.PropertyReader;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobjects.CheckoutPage;
import pageobjects.Login;
import pageobjects.PageObjectManager;

public class CheckoutStepDefTest extends Base  {
    CheckoutPage checkoutPage;
    WebDriver driver;
    Login login;
    PropertyReader prop;
    PageObjectManager pageObjectManager;


    @Then("Navigate to Checkout page")
    public void navigate_to_checkout_page() {
        pageObjectManager= PageObjectManager.getInstance();
        checkoutPage=pageObjectManager.getCheckoutPage(DriverManager.getDriver());
        checkoutPage.navigateToCheckoutPage();

    }

    @Then("Verify that the product Printed Chiffon Dress with the correct amount is displayed on Payment section")
    public void verify_that_the_product_printed_chiffon_dress_with_the_correct_amount_is_displayed_on_payment_section() {
        checkoutPage.validateDescriptionAndAmount();
    }

    @Then("Sing out")
    public void sing_out(){
        login.singOutSuccessfully();
    }
}
