package Stepdefinition;

import TestUtils.DriverManager;
import TestUtils.PropertyReader;
import io.cucumber.java.*;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Hooks{


    private DriverManager drivermanager;
    private WebDriver driver;
    private PropertyReader propertyReader;
    private static Scenario message;

    @Before
    public void beforeScenario()
    {
        propertyReader=new PropertyReader();
        String browser= propertyReader.properties.getProperty("browser");
        drivermanager=new DriverManager();
        drivermanager.init_driver(browser);
        DriverManager.getDriver().manage().window().maximize();
        DriverManager.getDriver().manage().deleteAllCookies();
    }

    @AfterStep
    public void takeScreenShotAfterEveryStep(Scenario scenario)
    {
        byte[] screenshot = ((TakesScreenshot)DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
        scenario.attach(screenshot, "image/png", "screenshot");
    }

    @After
    public void afterScenario()
    {
        drivermanager.getDriver().quit();
        drivermanager.init_driver("");
    }


}

