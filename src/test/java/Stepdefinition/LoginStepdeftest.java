package Stepdefinition;

import TestUtils.Base;
import TestUtils.DriverManager;
import TestUtils.PropertyReader;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageobjects.Login;
import pageobjects.PageObjectManager;

import java.io.IOException;
import java.sql.Driver;
import java.time.Duration;

public class LoginStepdeftest extends Base {

Login login;
PropertyReader propertyReader;
PageObjectManager pageObjectManager;
DriverManager drivermanager;




    @Given("User Login in")
    public void user_login_in() throws IOException {
        propertyReader=new PropertyReader();
        drivermanager=new DriverManager();
        drivermanager.getDriver().get(propertyReader.getEnvironment());
        drivermanager.getDriver().manage().timeouts().pageLoadTimeout(Duration.ofMinutes(3));

      pageObjectManager=PageObjectManager.getInstance();
      login=pageObjectManager.getLoginPage(DriverManager.getDriver());
      login.loginApplication();
    }

    @Then("Verify the login is Successful")
    public void verify_the_login_is_successful() {
        login.loginIsSuccessful();
    }

    @Then("Sign Out")
    public void signOut() {
        login.singOutSuccessfully();
    }

}
