package pageobjects;

import TestUtils.DriverManager;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class PageObjectManager {

    private WebDriver driver;
    public Login login;
    public Women women;
    public CheckoutPage checkoutPage;
    public DriverManager drivermanager;


    // private DailyIssuancePage dailyIssuancePage;

    private static PageObjectManager pageObjectManager;


    static {
        try {
            pageObjectManager = new PageObjectManager();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private PageObjectManager() throws IOException {
        driver = DriverManager.getDriver();
    }

    public static PageObjectManager getInstance() {
        return pageObjectManager;
    }


    public Login getLoginPage(WebDriver driver) {

        return login = new Login(driver);

    }

    public Women getWomenPage(WebDriver driver) {

        return women = new Women(driver);

    }
    public CheckoutPage getCheckoutPage(WebDriver driver){

        return checkoutPage = new CheckoutPage(driver);
    }

}
