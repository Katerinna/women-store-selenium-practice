package pageobjects;

import TestUtils.Base;
import TestUtils.PropertyReader;
import io.cucumber.java.bs.A;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CheckoutPage extends Base {

    @FindBy(xpath = "(//a[@title='Proceed to checkout'])[2]")
    WebElement CheckoutButtonStepOneAndTwo;

    @FindBy(xpath = "(//button[@type='submit'])[2]")
     WebElement CheckoutButtonStepThree;

    @FindBy(xpath = "//button[@name='processCarrier']")
    WebElement CheckoutButtonStepFour;

    @FindBy(xpath = "//a[@class='iframe']")
    WebElement ReadTermsOfService;

    @FindBy(xpath = "//a[@title='Close']")
     WebElement CloseBox;

    @FindBy(xpath = "//div[@id='uniform-cgv']")
    WebElement AgreeToTermsOfService;

    @FindBy(xpath = "//a[text()='Printed Chiffon Dress']")
     WebElement Description;

    @FindBy(id = "total_product_price_7_34_759632")
     WebElement Amount;

    WebDriver driver;
    PropertyReader prop;
    public CheckoutPage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
        prop=new PropertyReader();
    }

    public void navigateToCheckoutPage()
    {
        elementWaitToBeClickable(CheckoutButtonStepOneAndTwo);
        clickElement(CheckoutButtonStepOneAndTwo);
        elementWaitToBeClickable(CheckoutButtonStepThree);
        clickElement(CheckoutButtonStepThree);
        elementWaitVisibility(ReadTermsOfService);
        clickElement(ReadTermsOfService);
        elementWaitVisibility(CloseBox);
        clickElement(CloseBox);
        scrollDown(AgreeToTermsOfService);
        clickElement(AgreeToTermsOfService);
        clickElement(CheckoutButtonStepFour);

    }

    public void validateDescriptionAndAmount()
    {
        elementWaitVisibility(Description);
        Assert.assertTrue(Description.isDisplayed(), "Printed chiffon dress is present in description");
        Assert.assertNotNull(Description,"Element not found");

        elementWaitVisibility(Amount);
        Assert.assertTrue(Amount.isDisplayed(), "The correct amount is $16.40");
        Assert.assertNotNull(Amount,"Element not found");
    }


}
