package pageobjects;

import TestUtils.Base;
import TestUtils.PropertyReader;
import com.beust.ah.A;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.IOException;

public class Women extends Base {

    @FindBy(xpath="//a[text()='Women']")
    WebElement WomenTab;

    @FindBy(xpath = "(//a[text()='Dresses'])[2]")
    WebElement Dresses;

    @FindBy(xpath = "//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-line first-item-of-tablet-line last-mobile-line']")
    WebElement PrintedChiffonDress;

    @FindBy (xpath = "(//a[@title='Add to cart'])[5]")
     WebElement AddToCart;

    @FindBy(xpath = "//h2[contains(text()[2],'Product successfully added to your shopping cart')]")
    WebElement ProductSuccessfullyAddedToCartMessage;

    @FindBy(xpath = "//a[@title='Proceed to checkout']")
    WebElement CheckoutButton;

    WebDriver driver;
    PropertyReader prop;
    public Women(WebDriver driver)
    {
        super();
        this.driver=driver;
        PageFactory.initElements(driver,this);
        prop=new PropertyReader();
    }

    public void navigateToWomenPage()
    {
        mouseHoverClick(WomenTab);
    }

    public void addPrintedChiffonDressInTheCart()
    {
        elementWaitVisibility(Dresses);
        mouseHoverClick(Dresses);
        mouseHoverClick(PrintedChiffonDress);
        clickElement(AddToCart);
    }

    public void productSuccessfullyAddedToShoppingCart()
    {
        elementWaitVisibility(ProductSuccessfullyAddedToCartMessage);
        Assert.assertTrue(ProductSuccessfullyAddedToCartMessage.isDisplayed(), "Product added to your shopping cart");
        Assert.assertNotNull(ProductSuccessfullyAddedToCartMessage,"Element not found");

        clickElement(CheckoutButton);
    }
}
