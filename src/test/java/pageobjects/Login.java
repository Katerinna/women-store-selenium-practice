package pageobjects;

import TestUtils.Base;
import TestUtils.DriverManager;
import TestUtils.PropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.IOException;
import java.time.Duration;

public class Login extends Base {

  /*  private By signin_button= By.xpath("//a[@title='Log in to your customer account']");
    private By emailId= By.id("email");
    private By passwd= By.id("passwd");
    private By SubmitLogin= By.id("SubmitLogin");
    */

    @FindBy(xpath="//a[@title='Log in to your customer account']")
    WebElement signin_button;

    @FindBy(id="email")
    WebElement email;

    @FindBy(id="passwd")
    WebElement passwd;

    @FindBy(id="SubmitLogin")
    WebElement SubmitLogin;

    @FindBy(xpath="//p[text()='Welcome to your account. Here you can manage all of your personal information and orders.']")
     WebElement loginSuccessfulMessage;

    @FindBy(xpath = "//a[@class='logout']")
     WebElement logOut;

    WebDriver driver;
    PropertyReader prop;
    public Login(WebDriver driver) {
        this.driver=driver;
       PageFactory.initElements(driver,this);
        prop=new PropertyReader();
    }

    /**
     * To login in the application
     */
    public void loginApplication()

    {
        elementWaitVisibility(signin_button);
        clickElement(signin_button);
        email.sendKeys(prop.properties.getProperty("email"));
        passwd.sendKeys(prop.properties.getProperty("password"));
        clickElement(SubmitLogin);
    }

    public void loginIsSuccessful()
    {
        elementWaitVisibility(loginSuccessfulMessage);
        Assert.assertTrue(loginSuccessfulMessage.isDisplayed(), "Log in is successful");
        Assert.assertNotNull(loginSuccessfulMessage,"Element not found");

    }

    public void singOutSuccessfully()
    {
        elementWaitVisibility(logOut);
        clickElement(logOut);
        Assert.assertTrue(signin_button.isDisplayed(), "Log out is successful");
        Assert.assertNotNull(signin_button,"Element not found");

    }
}
