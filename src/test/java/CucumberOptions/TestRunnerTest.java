package CucumberOptions;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/java/features",
        glue={"Stepdefinition"},
        tags = "@smoke",
        plugin = "json:target/jsonreports/cucumber-report.json")

public class TestRunnerTest extends AbstractTestNGCucumberTests {
}
