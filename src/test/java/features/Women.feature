Feature: Test for products on Women page


  @smoke
  Scenario:Verify that user is able to add Printed Chiffon Dress from Women page

    Given User Login in
    And  On Women Page Add the Printed Chiffon Dress in the cart
    Then Verify that Product successfully added to your shopping cart
    And Navigate to Checkout page
    Then Verify that the product Printed Chiffon Dress with the correct amount is displayed on Payment section
    Then Sign Out