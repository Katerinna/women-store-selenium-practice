# Selenium Exercise

## Requirements

| Dependency   | Required (Version)  | 
|--------------|---------------------|
| JDK          | 16 or higher        |
| Apache Maven | 3.8.6               |

## Setup

[Install JDK](https://docs.oracle.com/en/java/javase/16/install/overview-jdk-installation.html)

[Install Maven](https://maven.apache.org/install.html)

Register free account in [automationpractice.com](http://automationpractice.com) and update email/password in `src/test/java/resources/test.properties` file.

Run in a console:
> mvn clean test

...or use your favorite IDE like IntellyJ IDEA

## Exercise

### Login.feature

1.Verify that on successful login user gets message as
Welcome to your account. Here you can manage all of your personal information and orders.

2.User should be able to sign out successfully

### Women.feature

1.On Women Page Add the Printed Chiffon Dress in the cart

2.On adding to cart verify the success message as Product successfully added to your shopping cart

3.Navigate to steps for 01 Summary to 04 Shipping

4.Verify that on 05 Payment, Printed Chiffon Dress is seen in description and correct amount as seen in cart

5.Create a report so that we are able to see the steps performed with screenshots for the test performed
